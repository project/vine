Vine Drupal Module
------------------
"Adds a formatter for text fields for display embedded Vines"
https://drupal.org/project/vine


This modules allows you to embed Vine's posts easily.

After installing this module, and enabled it...
2-steps Configuration:

In any content type use or add a Field of the type text.
Then, set Embed Vine Format on Display settings for chosen field.
Simply usage: Paste Vine's post URL on chosen field
e.g. https://vine.co/v/hmpvn259tjt

Further information:

This module comes with standard formats suggested by Vine.

You can choose between two styles: Simple and Postcard;
and between three sizes: Large (600px), Medium (480px) and Small (320px)

Include Vines on your articles and get great social video impact on your websites

A little experience with Drupal was needed for developing this simple module.
Base code was taken from this tutorial
Drupal 7 Tutorial: Creating Custom Formatters with the Field API
http://www.metaltoad.com/blog/drupal-7-tutorial-creating-custom-formatters
by Dann Linn https://drupal.org/user/566584
